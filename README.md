user
==========

Create a user and add him to groups

Role Variables
--------------

The desired behavior can be refined via variables.

Option | Description
--- | ---
`user_name` | Name of user to be created (default `selbstmade`)
`user_pass` | Password of the created user (default `somepass12345`)
`user_groups` | Groups to add user to (default `[]`)
`user_shell` | Users shell (default `/bin/bash`)
`public_keys` | List of public keys (default `~/.ssh/id_rsa.pub`)
`allow_passwordless_sudo` | Allow passwordless sudo for created user (default `true`)
`update_password` | Change user password (default `undefined`) - only executed of defined
`user_remove` | Remove the user (default `undefined`) - only executed of defined

For example, you can override default variables by passing it as a parameter to the role like so:

```yaml
roles:
    - { role: user, user_name: username2, user_pass: path/to/passwordfile }
```

Example Playbook
----------------

The example below uses `sudo` to play book on your localhost via local
connection.

```bash
ansible-playbook test.yml \
    -i hosts.example \
    -c local \
    -s --ask-sudo-pass
 ```

```yaml
# file: test.yml
- hosts: local

  vars:
    user_name: myusername
    user_pass: {{ my_vault_secret_user_password }}

  roles:
    - user
```
